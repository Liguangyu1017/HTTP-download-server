package cn.ykd.actualproject.model;

import java.io.Serializable;
import java.util.List;

/**
 * pagingModel
 */
public class Paging<R> implements Serializable {

    private static final long serialVersionUID = 522660448543880825L;
    /**
     * Number of pages
     */
    private int pageNum;

    /**
     * Quantity per page
     */
    private int pageSize = 15;
    /**
     * Total pages
     */
    private int totalPage;

    /**
     * Total records
     */
    private long totalCount;

    /**
     * Aggregate data
     */
    private List<R> data;

    public Paging() {

    }

    public Paging(int pageNum, int pageSize, int totalPage, long totalCount, List<R> data) {
        this.pageNum = pageNum;
        this.pageSize = pageSize;
        this.totalPage = totalPage;
        this.totalCount = totalCount;
        this.data = data;
    }

    public int getPageNum() {
        return pageNum;
    }

    public void setPageNum(int pageNum) {
        this.pageNum = pageNum;
    }

    public int getPageSize() {
        return pageSize;
    }

    public Paging setPageSize(int pageSize) {
        this.pageSize = pageSize;
        return this;
    }

    public int getTotalPage() {
        return totalPage;
    }

    public Paging setTotalPage(int totalPage) {
        this.totalPage = totalPage;
        return this;
    }

    public long getTotalCount() {
        return totalCount;
    }

    public Paging setTotalCount(long totalCount) {
        this.totalCount = totalCount;
        return this;
    }

    public List<R> getData() {
        return data;
    }

    public Paging setData(List<R> data) {
        this.data = data;
        return this;
    }

}