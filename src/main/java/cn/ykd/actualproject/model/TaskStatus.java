package cn.ykd.actualproject.model;

/**
 * taskStateEnumerationClass
 **/
public enum TaskStatus {
    // Download complete
    Downloaded,

    // Waiting
    Pending,

    // Cancel
    Canceled,

    // Downloading
    Downloading

}
