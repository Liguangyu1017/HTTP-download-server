package cn.ykd.actualproject.model;

public class Settings {

    //The path to be downloaded
    private String downloadPath;

    // Maximum number of download tasks
    private Integer maxTasks;

    // Fastest download speed
    private Double maxDownloadSpeed;

    // The fastest upload speed
    private Double maxUploadSpeed;

    public Settings() {
    }

    public Settings(String downloadPath, int maxTasks, Double maxDownloadSpeed, Double maxUploadSpeed) {
        this.downloadPath = downloadPath;
        this.maxDownloadSpeed = maxDownloadSpeed;
        this.maxUploadSpeed = maxUploadSpeed;
        this.maxTasks = maxTasks;
    }

    public String getDownloadPath() {
        return downloadPath;
    }

    public void setDownloadPath(String downloadPath) {
        this.downloadPath = downloadPath;
    }

    public Integer getMaxTasks() {
        return maxTasks;
    }

    public void setMaxTasks(int maxTasks) {
        this.maxTasks = maxTasks;
    }

    public Double getMaxDownloadSpeed() {
        return maxDownloadSpeed;
    }

    public void setMaxDownloadSpeed(Double maxDownloadSpeed) {
        this.maxDownloadSpeed = maxDownloadSpeed;
    }

    public Double getMaxUploadSpeed() {
        return maxUploadSpeed;
    }

    public void setMaxUploadSpeed(Double maxUploadSpeed) {
        this.maxUploadSpeed = maxUploadSpeed;
    }
}