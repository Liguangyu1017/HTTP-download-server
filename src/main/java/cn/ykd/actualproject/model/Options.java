package cn.ykd.actualproject.model;
/**
 * taskOperationEnumerationClass
 **/
public enum Options {
    /**
     * Continue downloading
     **/
    Resume,

    /**
     * Pause download
     **/
    Pause,

    /**
     * reDownload
     **/
    Refresh,

    /**
     * deleteDownload
     **/
    Delete,

    /**
     * cancelDownload
     **/
    Cancel
}
