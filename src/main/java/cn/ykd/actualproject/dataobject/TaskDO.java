package cn.ykd.actualproject.dataobject;

import cn.ykd.actualproject.model.Task;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.Serializable;
import java.util.Date;

/**
 * name: TaskDO
 * author: LiXingHao
 **/
public class TaskDO implements Serializable {

    // taskId
    private String id;

    //fileType
    private String type;

    //downloadLink
    private String url;

    //taskName
    private String name;

    //maximumNumberOfTaskThreads
    private Integer threads;

    //totalMemorySize
    private String size;

    //downloadingSpeed
    private String speed;

    //remainingTimeSeconds
    private Integer remainingTime;

    //downloadSpeed
    private String downloadSpeed;

    //downloadProgress
    private Integer progress;

    //node
    private Integer peers;

    //taskStatus
    private String status;

    //downloadPath
    private String path;

    // creationTime
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone="GMT+8")
    private Date gmtCreated;
    // completionTime
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone="GMT+8")
    private Date gmtModified;

    public TaskDO(String id, String type, String url, String name, Integer threads, String size, String speed, Integer remainingTime, String downloadSpeed, Integer progress, Integer peers, String status, Date gmtCreated, Date gmtModified) {
        this.id = id;
        this.type = type;
        this.url = url;
        this.name = name;
        this.threads = threads;
        this.size = size;
        this.speed = speed;
        this.remainingTime = remainingTime;
        this.downloadSpeed = downloadSpeed;
        this.progress = progress;
        this.peers = peers;
        this.status = status;
        this.gmtCreated = gmtCreated;
        this.gmtModified = gmtModified;
    }

    // The constructor takes the JSON string and parses it
public TaskDO(String jsonString) {
    try {
        ObjectMapper objectMapper = new ObjectMapper();
        TaskDO task = objectMapper.readValue(jsonString, TaskDO.class);
        // Assigns the property parsed from the JSON string to the current object
        this.id = task.getId();
        this.type = task.getType();
        this.url = task.getUrl();
        this.name = task.getName();
        this.threads = task.getThreads();
        this.size = task.getSize();
        this.speed = task.getSpeed();
        this.remainingTime = task.getRemainingTime();
        this.downloadSpeed = task.getDownloadSpeed();
        this.progress = task.getProgress();
        this.peers = task.getPeers();
        this.status = task.getStatus();
        this.gmtCreated = task.getGmtCreated();
        this.gmtModified = task.getGmtModified();
        // Handle other attributes as well...
    } catch (Exception e) {
        e.printStackTrace();
        // Handling exception
    }
}
    @Override
    public String toString() {
        return "TaskDO{" +
                "id='" + id + '\'' +
                ", type='" + type + '\'' +
                ", url='" + url + '\'' +
                ", name='" + name + '\'' +
                ", threads=" + threads +
                ", size='" + size + '\'' +
                ", speed='" + speed + '\'' +
                ", remainingTime=" + remainingTime +
                ", downloadSpeed='" + downloadSpeed + '\'' +
                ", progress=" + progress +
                ", peers=" + peers +
                ", status='" + status + '\'' +
                ", gmtCreated=" + gmtCreated +
                ", gmtModified=" + gmtModified +
                '}';
    }

    public TaskDO() {
    }

    public Task toModel(){
        Task task=new Task();
        task.setId(id);
        task.setType(type);
        task.setUrl(url);
        task.setName(name);
        task.setThreads(threads);
        task.setSize(size);
        task.setSpeed(speed);
        task.setRemainingTime(remainingTime);
        task.setDownloadSpeed(downloadSpeed);
        task.setProgress(progress);
        task.setPeers(peers);
        task.setPath(path);
        task.setStatus(status);
        task.setGmtCreated(gmtCreated);
        task.setGmtModified(gmtModified);
        return task;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getThreads() {
        return threads;
    }

    public void setThreads(Integer threads) {
        this.threads = threads;
    }

    public Integer getRemainingTime() {
        return remainingTime;
    }

    public void setRemainingTime(Integer remainingTime) {
        this.remainingTime = remainingTime;
    }

    public String getDownloadSpeed() {
        return downloadSpeed;
    }

    public void setDownloadSpeed(String downloadSpeed) {
        this.downloadSpeed = downloadSpeed;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getSpeed() {
        return speed;
    }

    public void setSpeed(String speed) {
        this.speed = speed;
    }

    public Integer getProgress() {
        return progress;
    }

    public void setProgress(Integer progress) {
        this.progress = progress;
    }

    public Integer getPeers() {
        return peers;
    }

    public void setPeers(Integer peers) {
        this.peers = peers;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getGmtCreated() {
        return gmtCreated;
    }

    public void setGmtCreated(Date gmtCreated) {
        this.gmtCreated = gmtCreated;
    }

    public Date getGmtModified() {
        return gmtModified;
    }

    public void setGmtModified(Date gmtModified) {
        this.gmtModified = gmtModified;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
}
