package cn.ykd.actualproject.dataobject;

import cn.ykd.actualproject.model.Settings;

import java.util.Date;

public class SettingsDO {
    //The path to be downloaded
    private String downloadPath;

    //Maximum number of download tasks
    private Integer maxTasks;

    //Fastest download speed
    private Double maxDownloadSpeed;

    //Fastest upload speed
    private Double maxUploadSpeed;

    //creationTime
    private Date gmtCreated;

    //modificationTime
    private Date gmtModified;


    public Date getGmtCreated() {
        return gmtCreated;
    }

    public void setGmtCreated(Date gmtCreated) {
        this.gmtCreated = gmtCreated;
    }

    public Date getGmtModified() {
        return gmtModified;
    }

    public void setGmtModified(Date gmtModified) {
        this.gmtModified = gmtModified;
    }

    public String getDownloadPath() {
        return downloadPath;
    }

    public void setDownloadPath(String downloadPath) {
        this.downloadPath = downloadPath;
    }

    public Integer getMaxTasks() {
        return maxTasks;
    }

    public void setMaxTasks(int maxTasks) {
        this.maxTasks = maxTasks;
    }

    public Double getMaxDownloadSpeed() {
        return maxDownloadSpeed;
    }

    public void setMaxDownloadSpeed(Double maxDownloadSpeed) {
        this.maxDownloadSpeed = maxDownloadSpeed;
    }

    public Double getMaxUploadSpeed() {
        return maxUploadSpeed;
    }

    public void setMaxUploadSpeed(Double maxUploadSpeed) {
        this.maxUploadSpeed = maxUploadSpeed;
    }

    public SettingsDO(Settings settings) {
        this.downloadPath = settings.getDownloadPath();
        this.maxDownloadSpeed = settings.getMaxDownloadSpeed();
        this.maxTasks = settings.getMaxTasks();
        this.maxUploadSpeed = settings.getMaxUploadSpeed();

    }

    public SettingsDO() {
        return;
    }

    public Settings convertToModel() {
        Settings settings = new Settings();
        settings.setMaxDownloadSpeed(maxDownloadSpeed);
        settings.setMaxTasks(maxTasks);
        settings.setMaxUploadSpeed(maxUploadSpeed);
        settings.setDownloadPath(downloadPath);
        return settings;
    }
}
