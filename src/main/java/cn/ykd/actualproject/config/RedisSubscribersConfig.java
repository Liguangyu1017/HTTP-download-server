package cn.ykd.actualproject.config;

import cn.ykd.actualproject.service.impl.RedisProducer;
import cn.ykd.actualproject.service.impl.RedisSubscriberCallback;
import cn.ykd.actualproject.service.impl.RedisSubscribersTaskOptions;
import cn.ykd.actualproject.service.impl.RedisSubscribersThreads;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.listener.ChannelTopic;
import org.springframework.data.redis.listener.RedisMessageListenerContainer;

@Configuration
public class RedisSubscribersConfig {

    @Bean
    public RedisMessageListenerContainer getRedisMessageListenerContainer(RedisConnectionFactory redisConnectionFactory,
                                                                          RedisSubscriberCallback redisSubscriberCallback,
                                                                          RedisSubscribersTaskOptions redisSubscribersTaskOptions,
                                                                          RedisSubscribersThreads redisSubscribersThreads) {
        RedisMessageListenerContainer redisMessageListenerContainer = new RedisMessageListenerContainer();
        redisMessageListenerContainer.setConnectionFactory(redisConnectionFactory);
        redisMessageListenerContainer.addMessageListener(redisSubscriberCallback, new ChannelTopic(RedisProducer.CALLBACK_KEY));
        redisMessageListenerContainer.addMessageListener(redisSubscribersTaskOptions, new ChannelTopic(RedisProducer.OPTIONS_KEY));
        redisMessageListenerContainer.addMessageListener(redisSubscribersThreads, new ChannelTopic(RedisProducer.THREADS_KEY));
        return redisMessageListenerContainer;
    }
}
