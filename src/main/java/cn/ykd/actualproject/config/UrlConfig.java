package cn.ykd.actualproject.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Configuration
public class UrlConfig {
    @Value("${thread.pool.size}")
    private int threadPoolSize;

    @Value("${min.chunk.size}")
    private int minChunkSize;

    @Value("${mid.chunk.size}")
    private int midChunkSize;

    @Value("${max.chunk.size}")
    private int maxChunkSize;

    public int getThreadPoolSize() {
        return threadPoolSize;
    }

    public int getMinChunkSize() {
        return minChunkSize;
    }

    public int getMidChunkSize() {
        return midChunkSize;
    }

    public int getMaxChunkSize() {
        return maxChunkSize;
    }
}
