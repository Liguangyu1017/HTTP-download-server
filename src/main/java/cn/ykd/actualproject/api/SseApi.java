package cn.ykd.actualproject.api;

import cn.ykd.actualproject.model.Task;
import cn.ykd.actualproject.service.SseService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;

@RestController
@RequestMapping("/sse")
public class SseApi {
    @Autowired
    private SseService sseService;

    private static final Logger LOG = LoggerFactory.getLogger(SseApi.class);

    // Create a connection
    @GetMapping("/event")
    public SseEmitter event() {
        return sseService.eventStream();
    }


}