package cn.ykd.actualproject.api;

import cn.ykd.actualproject.model.Result;
import cn.ykd.actualproject.param.FileParams;
import cn.ykd.actualproject.service.FileService;
import cn.ykd.actualproject.utils.FileInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.io.File;
import java.util.List;

@Controller
@RequestMapping("/api/file")
public class FileApi {
    private static final Logger LOG = LoggerFactory.getLogger(FileApi.class);
    @Autowired
    private FileService fileService;

    // getFileList
    @PostMapping("/list")
    @ResponseBody
    public Result<List<FileInfo>> fetchFileList(@RequestBody FileParams params,@RequestParam String fileName) {
        LOG.info("/api/file/list 已启动");
        Result<List<FileInfo>> result = fileService.fetchFileList(params,fileName);
        return result;
    }

}
