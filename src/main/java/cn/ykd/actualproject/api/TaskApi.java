package cn.ykd.actualproject.api;

import cn.ykd.actualproject.model.Paging;
import cn.ykd.actualproject.model.Result;
import cn.ykd.actualproject.model.Task;
import cn.ykd.actualproject.param.UrlRequestParams;
import cn.ykd.actualproject.service.TaskService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Controller
@RequestMapping("/api/task")
public class TaskApi {
    private static final Logger LOG = LoggerFactory.getLogger(TaskApi.class);

    @Autowired
    private TaskService taskService;

    //Pass in an address, launch the download module, and download the file.
    @PostMapping("/submit")
    @ResponseBody
    public Result<Task> submit(@RequestBody UrlRequestParams urlRequestParams) {
        LOG.info("/api/task/submit已启动");
        return taskService.submit(urlRequestParams);
    }


    /*
     * Update a thread of data
     */
    @PostMapping("/update_thread")
    @ResponseBody
    public Result<Task> update_thread(@RequestParam String id,@RequestParam Integer threads) {
        LOG.info("/api/task/update_thread 已启动");
        return taskService.update_thread(id,threads);
    }

    /*
     * Re-download the task in the ids list
     */
    @PostMapping("/refresh")
    @ResponseBody
    public Result<List<Task>> refresh(@RequestBody List<String> ids) {
        LOG.info("/api/task/refresh已启动");
        return taskService.refresh(ids);
    }

    /*
     * Stop downloading tasks in the ids list
     */
    @PostMapping("/pause")
    @ResponseBody
    public Result<List<Task>> pause(@RequestBody List<String> ids) {
        LOG.info("/api/task/pause已启动");
        return taskService.pause(ids);
    }

    /*
     * Continue downloading the tasks in the ids list
     */
    @PostMapping("/resume")
    @ResponseBody
    public Result<List<Task>> resume(@RequestBody List<String> ids) {
        LOG.info("/api/task/resume已启动");
        return taskService.resume(ids);
    }

    /*
     * Delete a task from the ids list
     */
    @PostMapping("/delete")
    @ResponseBody
    public Result<List<Task>> delete(@RequestBody List<String> ids) {
        LOG.info("/api/task/delete已启动");
        return taskService.delete(ids);
    }

    /*
     * The task list of Paging data type obtained after filtering filter conditions
     */
    @PostMapping(value = "/get_tasks", consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    @ResponseBody
    public Result<Paging<Task>> get_tasks(@RequestParam("currentPage") Integer currentPage, @RequestParam("pageSize") Integer pageSize, @RequestParam("filter") String filter) {
        LOG.info("/api/task/get_tasks已启动");
        return taskService.get_tasks(currentPage,pageSize,filter);
    }
}
