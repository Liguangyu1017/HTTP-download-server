package cn.ykd.actualproject.api;

import cn.ykd.actualproject.model.Settings;
import cn.ykd.actualproject.model.Result;
import cn.ykd.actualproject.service.SettingsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/settings")
public class SettingsApi {
    private static final Logger LOG = LoggerFactory.getLogger(SettingsApi.class);

    @Autowired
    private SettingsService settingsService;

    // getConfigurationInstance
    @GetMapping("/get")
    public Result<Settings> getSettings() {
        LOG.info("/api/settings/get 已启动");
        return settingsService.getSettings();
    }

    // A settings instance is passed to the back end to modify the default configuration
    @PostMapping("/save")
    public Result<Settings> saveSettings(@RequestBody Settings settings) {
        LOG.info("/api/settings/save 已启动");
        return settingsService.updateSettings(settings);
    }
}
