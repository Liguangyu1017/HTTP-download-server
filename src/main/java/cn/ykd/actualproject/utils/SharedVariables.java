package cn.ykd.actualproject.utils;

/**
 * Shared variable
 */
public class SharedVariables {
    /**
     * Whether to suspend download
     */
    public static boolean isPaused = false;
    /**
     * Whether to download it for the first time
     */
    public static boolean isFirst = true;

    public static boolean getIsPaused() {
        return isPaused;
    }

    public static void setIsPaused(boolean isPaused) {
        SharedVariables.isPaused = isPaused;
    }

    public static boolean getIsFirst() {
        return isFirst;
    }

    public static void setIsFirst(boolean isFirst) {
        SharedVariables.isFirst = isFirst;
    }
}
