package cn.ykd.actualproject.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

public class FileInfo {
    private String fileName;
    private String fileSize;
    private String gmtModified;
    private Boolean isDirectory;

    public FileInfo(String fileName, long fileSizeBytes, Date gmtModified,Boolean isDirectory) {
        this.fileName = fileName;
        if (fileSizeBytes >= 0) {
            if (fileSizeBytes <= 1024 * 1024) {
                // If the file size is less than or equal to 1 MB, the file is converted to KB
                double fileSizeKB = (double) fileSizeBytes / 1024;
                this.fileSize = String.format("%.1f", fileSizeKB) + " KB";
            } else {
                // If the file size is larger than 1 MB, convert it to MB
                double fileSizeMB = (double) fileSizeBytes / (1024 * 1024);
                this.fileSize = String.format("%.1f", fileSizeMB) + " MB";
            }
        } else {
            this.fileSize = null; // The folder is set to null
        }
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        this.gmtModified = dateFormat.format(gmtModified);
        this.isDirectory = isDirectory;
    }

    public String getFileName() {
        return fileName;
    }

    public String getFileSize() {
        return fileSize;
    }

    public String getGmtModified() {
        return gmtModified;
    }

    public Boolean getDirectory() {
        return isDirectory;
    }

    public void setDirectory(Boolean directory) {
        isDirectory = directory;
    }
}
