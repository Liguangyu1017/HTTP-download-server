package cn.ykd.actualproject.service;

import cn.ykd.actualproject.model.Result;
import cn.ykd.actualproject.model.Settings;

public interface SettingsService {
    // Get the current configuration
    Result<Settings> getSettings();
    // Update the configuration
    Result<Settings> updateSettings(Settings settings);
}
