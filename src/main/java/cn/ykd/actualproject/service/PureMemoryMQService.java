package cn.ykd.actualproject.service;

import java.util.List;

/**
 * @apiNote java implementation of the pseudo-Redis scoreboard
 */
public interface PureMemoryMQService {

    /**
    * @apiNote initializes the task scoreboard
    * @param taskId Indicates the task id
    * @param chunkNum Number of fragments
     **/
    void initializeScoreboardPure(String taskId, long chunkNum);

    /**
     * @apiNote updates the quest scoreboard
     * @param taskId Indicates the task id
     * @param chunkId Fragment id
     **/
    void updateScoreboardPure(String taskId, long chunkId);

    /**
     * @param taskId Indicates the task id
     * @return Fragment Id list
     * @apiNote gets the quest scoreboard
     **/
    List<Long> getScoreboardPure(String taskId);


    /**
     * @param taskId Indicates the task id
     * @apiNote deletes the scoreboard for the task
     **/
    boolean deleteScoreboard(String taskId);
}
