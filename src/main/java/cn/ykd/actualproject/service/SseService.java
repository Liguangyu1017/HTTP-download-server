package cn.ykd.actualproject.service;

import cn.ykd.actualproject.model.Task;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;

public interface SseService {

    /**
     * Send file download progress Download speed to the front end
     * @param
     * @return
     */
    SseEmitter eventStream();


    void getDownloadInfo(Task task);

}