package cn.ykd.actualproject.service;

import cn.ykd.actualproject.dataobject.TaskDO;

import java.util.List;

public interface RedisService {

    /**
     * @apiNote Parallel downloading using pub/sub message queues
     **/
    public void parallelDownloadExecution(TaskDO taskDO);

    /**
     * @deprecated
     * @apiNote Single task download using List message queue
     **/
    public void downloadExecution(TaskDO taskDO);

    /**
     * @apiNote initializes the task scoreboard
     * @param taskId Indicates the task id
     * @param chunkNum Number of fragments
     **/
    void initializeScoreboard(String taskId,long chunkNum);

    /**
     * @apiNote updates the quest scoreboard
     * @param taskId Indicates the task id
     * @param chunkId Fragment id
     **/
    void updateScoreboard(String taskId,long chunkId);

    /**
     * @param taskId Indicates the task id
     * @return Fragment Id list
     * @apiNote gets the quest scoreboard
     **/
    List<Long> getScoreboard(String taskId);

    /**
     * @param taskId Indicates the task id
     * @apiNote deletes the scoreboard for the task
     **/
    boolean deleteScoreboard(String taskId);

    /**
     * @deprecated
     * @param taskDO Task instance
     * @apiNote returns the status of the task download module to SSE
     **/
    public void callbackExecution(TaskDO taskDO);
}
