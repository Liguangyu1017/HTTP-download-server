package cn.ykd.actualproject.service.impl;

import cn.ykd.actualproject.dao.TaskDAO;
import cn.ykd.actualproject.dataobject.TaskDO;
import cn.ykd.actualproject.model.Task;
import cn.ykd.actualproject.service.SseService;
import cn.ykd.actualproject.utils.UUIDUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicBoolean;


@Service
public class SseServiceImpl implements SseService {

    @Autowired
    private TaskDAO taskDAO;

    //Ensure thread safety
    private static final Map<String, SseEmitter> emitters = new ConcurrentHashMap<>();

    private static final Logger LOG = LoggerFactory.getLogger(SseServiceImpl.class);

    private AtomicBoolean continueTask = new AtomicBoolean(true);

    @Override
    public SseEmitter eventStream() {

        // Set a long timeout value
        SseEmitter sseEmitter = new SseEmitter(0L);
        String clientId = UUIDUtils.getUUID();
        // Life cycle
        sseEmitter.onCompletion(() -> emitters.remove(clientId));
        sseEmitter.onError((e) -> {
            LOG.warn("sse broken clientId:" + clientId, e);
            emitters.remove(clientId);
        });
        sseEmitter.onTimeout(() -> emitters.remove(clientId));
        emitters.put(clientId, sseEmitter);
        LOG.info("create clientId:" + clientId);
        return sseEmitter;
    }


    /**
     * Gets the data passed by the task module
     *
     * @param
     */
    @Override
    public void getDownloadInfo(Task task) {
        Map<String, String> data = new HashMap<>();
        try {
            data.put("downloadTaskId", task.getId());
            data.put("progress", task.getProgress().toString());
            data.put("status", task.getStatus());
            data.put("downloadSpeed", task.getDownloadSpeed());
            data.put("remainingTime", task.getRemainingTime().toString());
        }catch (NullPointerException ignored) {}
        for (Map.Entry<String, SseEmitter> entry : emitters.entrySet()) {
            String clientId = entry.getKey();
            SseEmitter conn = entry.getValue();
            try {
                conn.send(SseEmitter.event().id(task.getId()).data(data, MediaType.APPLICATION_JSON));
            } catch (IOException e) {
                LOG.warn("send task fail, clientId" + clientId + "taskId" + task.getId(), e);
            }
        }
    }

    @Scheduled(fixedRate = 500)
    public void pushEvent() {
        List<TaskDO> taskDOS = taskDAO.selectDownloadingTasks();
        for (TaskDO taskDO : taskDOS) {
            Task task = taskDO.toModel();
            if (task.getStatus().equals("Downloaded")){
                continueTask.set(false);
                getDownloadInfo(task);
            }
            getDownloadInfo(task);
        }
    }
}
