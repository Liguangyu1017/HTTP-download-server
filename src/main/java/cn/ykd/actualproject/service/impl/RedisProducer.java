package cn.ykd.actualproject.service.impl;

import cn.ykd.actualproject.dataobject.TaskDO;
import cn.ykd.actualproject.param.OptionsParams;
import cn.ykd.actualproject.param.ThreadsParams;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

@Service
public class RedisProducer {
    // Callback task channel name
    public static final String CALLBACK_KEY = "CALLBACK_QUEUE";
    // Task status channel name
    public static final String OPTIONS_KEY = "OPTIONS_QUEUE";
    // Task thread channel name
    public static final String THREADS_KEY = "THREADS_QUEUE";
    private static final Logger LOG= LoggerFactory.getLogger(RedisProducer.class);

    @Autowired
    private RedisTemplate<String, Object> redisTemplate;

    /**
     * @deprecated
     * @apiNote The producer passes in the task data of the callback
     **/
    public void produceCallBack(TaskDO taskDO) {
        String jsonTaskDO=transformObjectToJson(taskDO);
        redisTemplate.convertAndSend(CALLBACK_KEY,jsonTaskDO);
        LOG.info(RedisProducer.CALLBACK_KEY +"通道发布数据："+taskDO.toString());
    }

    /**
     * @param optionsParams Pass in a list of task ids with the task state to change
     **/
    public void produceOptions(OptionsParams optionsParams) {
        String jsonTaskDO=transformObjectToJson(optionsParams);
        redisTemplate.convertAndSend(OPTIONS_KEY,jsonTaskDO);
        LOG.info(RedisProducer.OPTIONS_KEY+"通道发布数据：");
        LOG.info("ids -- "+optionsParams.getIds().toString());
        LOG.info("option -- " +optionsParams.getOption());
    }

    /**
     * @param threadsParams Pass in the task id and the number of threads to update
     **/
    public void produceThreads(ThreadsParams threadsParams) {
        String jsonTaskDO=transformObjectToJson(threadsParams);
        redisTemplate.convertAndSend(THREADS_KEY,jsonTaskDO);
        LOG.info(RedisProducer.THREADS_KEY+"通道发布数据：");
        LOG.info("id -- "+threadsParams.getId());
        LOG.info("threads -- "+threadsParams.getThreads());
    }

    /**
     * Object->json converter
     **/
    private String transformObjectToJson(Object object){
        ObjectMapper objectMapper=new ObjectMapper();
        try{
            return objectMapper.writeValueAsString(object);
        }catch (JsonProcessingException e){
            throw new RuntimeException("转换Json格式失败: " + e.getMessage());
        }
    }
}
