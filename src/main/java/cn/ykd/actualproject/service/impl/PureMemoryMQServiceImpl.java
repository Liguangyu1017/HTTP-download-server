package cn.ykd.actualproject.service.impl;

import cn.ykd.actualproject.service.PureMemoryMQService;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

@Component
@EnableScheduling
public class PureMemoryMQServiceImpl implements PureMemoryMQService {
    private static final Map<String, Map<String,Boolean>> scoreboard = new ConcurrentHashMap<>();

    @Override
    public void initializeScoreboardPure(String taskId,long chunkNum){
        Map<String,Boolean> chunkMap=new HashMap<>();
        for (long i = 0;i < chunkNum; i++){
            chunkMap.put(String.valueOf(i),false);
        }
        scoreboard.put(taskId,chunkMap);
    }

    @Override
    public void updateScoreboardPure(String taskId, long chunkId) {
        Map<String,Boolean> chunkMap=scoreboard.get(taskId);
        chunkMap.put(String.valueOf(chunkId),true);
        scoreboard.put(taskId,chunkMap);
    }

    @Override
    public List<Long> getScoreboardPure(String taskId){
        List<Long> chunkIds=new ArrayList<>();
        Map<String,Boolean> chunkMap=scoreboard.get(taskId);
        chunkMap.forEach((key,value)->{
            if (!value){
                chunkIds.add(Long.valueOf(key));
            }
        });
        return chunkIds;
    }

    @Override
    public boolean deleteScoreboard(String taskId){
        return !scoreboard.remove(taskId).isEmpty();
    }

}
