package cn.ykd.actualproject.service.impl;

import cn.ykd.actualproject.dao.TaskDAO;
import cn.ykd.actualproject.dataobject.TaskDO;
import cn.ykd.actualproject.service.DownloadService;
import cn.ykd.actualproject.service.RedisService;
import cn.ykd.actualproject.service.TaskManagerService;
import cn.ykd.actualproject.utils.SharedVariables;
import com.google.common.util.concurrent.RateLimiter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.io.File;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.atomic.AtomicLong;

@Service
public class TaskManagerServiceImpl implements TaskManagerService {
    @Autowired
    private TaskDAO taskDAO;
    @Autowired
    private RedisSubscribersTaskOptions redisSubscribersTaskOptions;
    @Autowired
    private DownloadService downloadService;
    @Autowired
    private RedisService redisService;
    @Autowired
    private RedisTemplate redisTemplate;
    @Autowired
    private RedisSubscribersThreads redisSubscribersThreads;
    private static final String KEY_MESSAGE_PARALLEL_DOWNLOAD = "DOWNLOAD_THREAD_QUEUE";
    private static final String KEY_CHUNK_HASHMAP = "CHUNK_HASHMAP";
    private static final Logger LOG = LoggerFactory.getLogger(TaskManagerServiceImpl.class);

    @Override
    public void updateDownloadStatus(ExecutorService executor, AtomicLong totalDownloadedBytes, long fileSize,
                                     TaskDO taskDO, long startTime, File outputFile, long chunkNum, long chunkSize,
                                     long[] chunkDownloadBytes, long[] chunkDownloadBytesShould, RateLimiter rateLimiter,
                                     long[] sp, long[] ep) {

        ExecutorService statusUpdater = Executors.newSingleThreadExecutor();
        ExecutorService newExecutor = Executors.newFixedThreadPool(taskDO.getThreads());
        String urlString = taskDO.getUrl();

        statusUpdater.submit(() -> {
            while (true) {
                // Calculate download information
                long downloadedBytes = totalDownloadedBytes.get();
                long currentTime = System.currentTimeMillis();
                long elapsedTime = currentTime - startTime;
                double downloadSpeedMBps = (downloadedBytes / 1024.0 / 1024.0) / (elapsedTime / 1000.0);
                int remainingTimeSeconds = (int) (((fileSize - downloadedBytes) / 1024.0 / 1024.0) / downloadSpeedMBps);
                int progressPercent = (int) ((downloadedBytes / (fileSize * 1.0)) * 100);

                ThreadPoolExecutor threadPoolExecutor = (ThreadPoolExecutor) executor;
                threadPoolExecutor.setMaximumPoolSize(10);
                Integer threadNum = redisSubscribersThreads.getThreadsForId(taskDO.getId());
                if (threadNum != null) {
                    threadPoolExecutor.setCorePoolSize(threadNum); // Dynamically resize thread pools
                }

                LOG.info("id:" + taskDO.getId() + ",remainingTime:" + remainingTimeSeconds + "s," +
                        "speed:" + String.format("%.2f", downloadSpeedMBps) + "MB/s,progress:" + progressPercent);

                int isUpdate = taskDAO.updateDownloadProgress(taskDO.getId(),
                        String.format("%.2f MB/s", downloadSpeedMBps),
                        remainingTimeSeconds,
                        progressPercent);

                /*
                    Delete: The update fails, that is, isUpdate == 0 is triggered
                    step1: Close all download threads
                    step2: Delete the local file
                 */
                if (isUpdate == 0) {
                    LOG.info("正在执行删除任务操作");
                    pauseDownload(executor);
                    deleteFile(outputFile);
                    deleteRedisInfo(taskDO);
                    statusUpdater.shutdown();
                    break;
                }
                // Get task status
                String status = redisSubscribersTaskOptions.getOptionsForId(taskDO.getId());

                /*
                 * PAUSE download: Triggered when the task obtained from redis is in Pause state
                 * step1: Close all download threads
                 * step2: Update the scoreboard
                 */
                if (status != null && status.equals("Pause")) {
                    LOG.info("正在执行暂停下载操作");
                    pauseDownload(executor);
                    status = null;
                    // Do not exit the loop until the task state changes
                    while (status == null) {
                        try {
                            Thread.sleep(500);
                            status = redisSubscribersTaskOptions.getOptionsForId(taskDO.getId());
                        } catch (InterruptedException | NullPointerException e) {
                            throw new RuntimeException(e);
                        }
                    }

                    // After a task is suspended, you can delete it, resume downloading it, and download it again
                    if (status.equals("Delete")) {
                        LOG.info("正在执行删除任务操作");
                        deleteFile(outputFile);
                        deleteRedisInfo(taskDO);
                        statusUpdater.shutdown();
                        break;
                    } else if (status.equals("Resume")) {
                        /*
                        RESUME download: triggered when the status of the task obtained from redis is Resume
                        Step p1: Obtain the undownloaded fragment Id from redis
                        step2: Enumerate all ids in step1, and recalculate sp and ep.(sp = original sp + number of
                               bytes downloaded before the current fragment is paused, ep remains unchanged)
                        step3: Submit to the download thread pool and recurse the updateDownloadStatus method
                     */
                        LOG.info("正在执行恢复下载操作");
                        // Obtain the id of the unfinished fragment
                        List<Long> unDownloadedChunkId = redisService.getScoreboard(taskDO.getId());
                        // Submit the unfinished shard to the thread pool
                        for (int i = 0; i < unDownloadedChunkId.size(); i++) {
                            int index = Math.toIntExact(unDownloadedChunkId.get(i));
                            long downloadedBytesOneChunk = chunkDownloadBytes[index];

                            long resumeSp = sp[index] + chunkDownloadBytes[index];
                            long resumeEp = ep[index];

                            newExecutor.submit(new DownloadServiceImpl.Download(urlString, outputFile.getPath(), resumeSp,
                                    resumeEp, totalDownloadedBytes,
                                    downloadedBytesOneChunk, index, rateLimiter, taskDO.getId(), redisService));
                        }
                        updateDownloadStatus(newExecutor, totalDownloadedBytes, fileSize, taskDO, startTime, outputFile,
                                chunkNum, chunkSize, chunkDownloadBytes, chunkDownloadBytesShould, rateLimiter, sp, ep);
                        statusUpdater.shutdown();
                        break;
                    } else {
                        refreshDownload(taskDO, executor, statusUpdater, outputFile);
                        break;
                    }
                }


                /*
                 * Re-download: triggered when the task status obtained from redis is REFRESH
                 * step1: Close all download threads
                 * step2: Delete the local file
                 * step3: Recursive download method
                 */
                if (status != null && status.equals("Refresh")) {
                    SharedVariables.setIsPaused(true);
                    executor.shutdownNow();

                    while (!executor.isTerminated()) {
                        try {
                            Thread.sleep(200);
                        } catch (InterruptedException e) {
                            throw new RuntimeException(e);
                        }
                    }

                    // reDownload
                    refreshDownload(taskDO, executor, statusUpdater, outputFile);
                    break;
                }

                // File download complete
                if (downloadedBytes == fileSize) {
                    LOG.info("文件下载完成. 文件保存为: " + outputFile.getAbsolutePath());

                    // clean resources
                    deleteRedisInfo(taskDO);
                    totalDownloadedBytes.set(0);
                    executor.shutdown();
                    statusUpdater.shutdown();
                    break;
                }
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    LOG.error(Thread.currentThread().getName() + "被中断了");
                }
            }
        });

    }


    // Delete redis information
    private void deleteRedisInfo(TaskDO taskDO) {
        // Delete task status
        if (redisTemplate.opsForHash().delete(RedisProducer.OPTIONS_KEY, taskDO.getId()) != 0L) {
            LOG.info(taskDO.getId() + "任务状态已删除");
        }
        // Delete thread instance
        if (redisTemplate.opsForHash().delete(KEY_MESSAGE_PARALLEL_DOWNLOAD, taskDO.getId()) != 0L) {
            LOG.info(taskDO.getId() + "并行线程已删除");
        }
        // Delete the task scoreboard
        if (redisTemplate.opsForHash().delete(KEY_CHUNK_HASHMAP, taskDO.getId()) != 0L) {
            LOG.info(taskDO.getId() + "任务记分牌已删除");
        }
    }

    // Pause download
    private void pauseDownload(ExecutorService executor) {
        SharedVariables.setIsPaused(true);
        // If downloading for the first time
        executor.shutdownNow();
        // Until the download thread closes
        while (!executor.isTerminated()) {
            try {
                Thread.sleep(200);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
        LOG.info("任务已暂停");
        // Reset is Pause
        SharedVariables.setIsPaused(false);
    }

    // reDownload
    private void refreshDownload(TaskDO taskDO, ExecutorService executor, ExecutorService statusUpdater, File outputFile) {
        LOG.info("正在执行重新下载操作");
        LOG.info("线程关闭中");
        if (executor.isTerminated()) {
            LOG.info("所有线程已关闭");
            deleteFile(outputFile);
            SharedVariables.setIsPaused(false);
        }
        downloadService.download(taskDO);
        statusUpdater.shutdown();
    }

    // Delete temporary files
    private void deleteFile(File outputFile) {
        if (outputFile.delete()) {
            LOG.info("文件已删除");
        } else {
            LOG.info("文件删除失败,请手动删除");
        }
    }
}
