package cn.ykd.actualproject.service.impl;

import cn.ykd.actualproject.dao.TaskDAO;
import cn.ykd.actualproject.dataobject.TaskDO;
import cn.ykd.actualproject.model.*;
import cn.ykd.actualproject.param.OptionsParams;
import cn.ykd.actualproject.param.TaskParams;
import cn.ykd.actualproject.param.ThreadsParams;
import cn.ykd.actualproject.param.UrlRequestParams;
import cn.ykd.actualproject.service.TaskService;
import cn.ykd.actualproject.utils.UUIDUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;


@Component
public class TaskServiceImpl implements TaskService {
    @Autowired
    private RedisServiceImpl redisService;
    @Autowired
    private TaskDAO taskDAO;
    @Autowired
    private RedisProducer redisProducer;
    @Autowired
    private RedisTemplate redisTemplate;
    private static final Logger LOG = LoggerFactory.getLogger(TaskServiceImpl.class);
    private static final String KEY_MESSAGE_PARALLEL_DOWNLOAD = "DOWNLOAD_THREAD_QUEUE";
    private static final String KEY_CHUNK_HASHMAP = "CHUNK_HASHMAP";
    /**
     * Default task thread
     */
    private static final int DEFAULT_TASK_THREADS =4;
    /**
     * Maximum task thread
     */
    private static final int MAX_TASK_THREADS = 10;

    public Result<Task> submit(UrlRequestParams urlRequestParams) {
        Result<Task> result = new Result<>();
        if (StringUtils.isEmpty(urlRequestParams.getUrl())) {
            result.setCode("409");
            result.setMessage("path不能为空");
            return result;
        }
                RestTemplate restTemplate = new RestTemplate();
                try {
                    restTemplate.headForHeaders(urlRequestParams.getUrl());
                    LOG.info("HTTP连接成功：" + urlRequestParams.getUrl());
                } catch (HttpClientErrorException e) {
                    LOG.error("HTTP连接失败：" + urlRequestParams.getUrl());
                    result.setCode("412");
                    result.setMessage("文件地址格式错误");
                    return result;
                }
        //Gets the file name from a URL
        String name = urlRequestParams.getUrl().substring(urlRequestParams.getUrl().lastIndexOf('/') + 1);
        TaskDO taskDO = new TaskDO();
        if (taskDAO.findByUrl(urlRequestParams.getUrl()) != null) {
            //We already have this download task
            int urlNum=taskDAO.findNumByUrl(urlRequestParams.getUrl())+1;
            //Press. To split name
            String[] urlList=name.split("\\.");
            StringBuilder newName= new StringBuilder();
            for (int i=0;i<urlList.length;i++) {
                if (i==urlList.length-1){
                    taskDO.setType(urlList[urlList.length - 1]);
                    newName.deleteCharAt(newName.length()-1);
                    newName.append("(").append(urlNum).append(").").append(urlList[urlList.length - 1]);
                }else {
                    //Adds the specified content to the end of the String Builder and returns the String Builder object itself
                    newName.append(urlList[i]).append(".");
                }
            }
            taskDO.setName(String.valueOf(newName));
        }else {
            taskDO.setName(name);
        }
        taskDO.setUrl(urlRequestParams.getUrl());
        taskDO.setId(UUIDUtils.getUUID());
        taskDO.setThreads(DEFAULT_TASK_THREADS);
        if (taskDAO.insert(taskDO) > 0) {
            result.setSuccess(true);
            result.setData(taskDAO.findById(taskDO.getId()).toModel());
            redisService.parallelDownloadExecution(taskDO);
        } else {
            result.setMessage("插入taskDO失败");
            result.setCode("410");
        }
        return result;
    }

    public Result<List<Task>> refresh(List<String> ids) {
        Result<List<Task>> result = new Result<>();
        if (CollectionUtils.isEmpty(ids)) {
            result.setMessage("ids为空");
            result.setCode("407");
            return result;
        }
        List<Task> tasks = new ArrayList<>();
        for (String id : ids) {
            TaskDO taskDO = taskDAO.findById(id);
            if (taskDO != null) {
                if (taskDO.getStatus().equals(TaskStatus.Downloaded.toString())) {
                    LOG.info(id + "任务已下载完毕，删除原任务记录，执行重新下载");
                    taskDAO.deleteByName(taskDO.getName());
                    submit(new UrlRequestParams(taskDO.getUrl()));
                } else {
                    taskDAO.refresh(id);
                }
                tasks.add(taskDO.toModel());
            } else {
                result.setMessage("id为" + id + "任务不存在");
                result.setCode("408");
                return result;
            }
        }
        List<String> newIds=new ArrayList<>();
        tasks.stream().filter(task -> !task.getStatus().equals(TaskStatus.Downloaded.toString()))
                .forEach(task -> newIds.add(task.getId()));
        if (!newIds.isEmpty()) {
            redisProducer.produceOptions(new OptionsParams(newIds,Options.Refresh));
        }
        result.setSuccess(true);
        result.setData(tasks);
        return result;
    }

    public Result<List<Task>> pause(List<String> ids) {
        Result<List<Task>> result = new Result<>();
        if (CollectionUtils.isEmpty(ids)) {
            result.setMessage("ids为空");
            result.setCode("407");
            return result;
        }
        List<Task> tasks = new ArrayList<>();
        for (String id : ids) {
            if (taskDAO.pause(id) > 0) {
                tasks.add(taskDAO.findById(id).toModel());
            } else {
                result.setMessage("id为" + id + "任务不存在");
                result.setCode("408");
                return result;
            }
        }
        List<String> newIds=new ArrayList<>();
        tasks.forEach(task -> newIds.add(task.getId()));
        redisProducer.produceOptions(new OptionsParams(newIds,Options.Pause));
        result.setSuccess(true);
        result.setData(tasks);
        return result;
    }

    public Result<List<Task>> resume(List<String> ids) {
        Result<List<Task>> result = new Result<>();
        if (CollectionUtils.isEmpty(ids)) {
            result.setMessage("ids为空");
            result.setCode("407");
            return result;
        }
        List<Task> tasks = new ArrayList<>();
        for (String id : ids) {
            if (taskDAO.resume(id) > 0) {
                tasks.add(taskDAO.findById(id).toModel());
            } else {
                result.setMessage("id为" + id + "任务不存在");
                result.setCode("408");
                return result;
            }
        }
        List<String> newIds=new ArrayList<>();
        tasks.forEach(task -> newIds.add(task.getId()));
        redisProducer.produceOptions(new OptionsParams(newIds,Options.Resume));
        result.setSuccess(true);
        result.setData(tasks);
        return result;
    }

    public Result<List<Task>> cancel(List<String> ids) {
        Result<List<Task>> result = new Result<>();
        if (CollectionUtils.isEmpty(ids)) {
            result.setMessage("ids为空");
            result.setCode("407");
            return result;
        }
        List<Task> tasks = new ArrayList<>();
        for (String id : ids) {
            if (taskDAO.cancel(id) > 0) {
                tasks.add(taskDAO.findById(id).toModel());
            } else {
                result.setMessage("id为" + id + "任务不存在");
                result.setCode("408");
                return result;
            }
        }
        List<String> newIds=new ArrayList<>();
        tasks.forEach(task -> newIds.add(task.getId()));
        redisProducer.produceOptions(new OptionsParams(newIds,Options.Cancel));
        result.setSuccess(true);
        result.setData(tasks);
        return result;
    }

    public Result<List<Task>> delete(List<String> ids) {
        Result<List<Task>> result = new Result<>();
        if (CollectionUtils.isEmpty(ids)) {
            result.setMessage("ids为空");
            result.setCode("407");
            return result;
        }
        for (String id : ids) {
            if (taskDAO.delete(id) < 1) {
                result.setMessage("id为" + id + "任务不存在");
                result.setCode("408");
                return result;
            }

            if (redisTemplate.opsForHash().delete(RedisProducer.OPTIONS_KEY,id)!=0L){
                LOG.info(id+"任务状态已删除");
            }
            // Delete thread instance
            if(redisTemplate.opsForHash().delete(KEY_MESSAGE_PARALLEL_DOWNLOAD, id)!=0L){
                LOG.info(id+"并行线程已删除");
            }
            // Delete the task scoreboard
            if(redisTemplate.opsForHash().delete(KEY_CHUNK_HASHMAP,id)!=0L){
                LOG.info(id+"任务记分牌已删除");
            }
        }
        redisProducer.produceOptions(new OptionsParams(ids,Options.Delete));
        result.setSuccess(true);
        return result;
    }

    @Override
    public Result<Task> update_thread(String id, Integer threads) {
        Result<Task> result = new Result<>();
        if (StringUtils.isEmpty(id) || threads == null) {
            LOG.info("taskId或thread不存在");
            result.setMessage("taskId或thread不存在");
            result.setCode("400");
            return result;
        }

        if (threads < 1 || threads > MAX_TASK_THREADS) {
            result.setMessage("任务线程数不符合");
            result.setCode("401");
            return result;
        }
        TaskDO taskDO = taskDAO.findById(id);
        if (taskDO == null) {
            result.setMessage("task不存在");
            result.setCode("403");
            return result;
        }
        if (taskDAO.updateThreads(id, threads) > 0) {
            result.setData(taskDAO.findById(id).toModel());
            result.setSuccess(true);
            redisProducer.produceThreads(new ThreadsParams(id,threads));
        } else {
            result.setMessage("数据库未更新");
            result.setCode("402");
        }
        return result;
    }

    @Override
    public Result<Paging<Task>> get_tasks(Integer currentPage, Integer pageSize, String filter) {
        Result<Paging<Task>> result = new Result<>();
        Paging<Task> paging = new Paging<>();
        TaskParams taskParams = new TaskParams();
        List<String> list = new ArrayList<>();
        for (TaskStatus taskStatus : TaskStatus.values()) {
            list.add(taskStatus.toString());
        }
        if (StringUtils.isEmpty(filter)) {
            result.setCode("406");
            result.setMessage("filter不存在");
            return result;
        } else if (!list.contains(filter) && !filter.equals("ALL")) {
            result.setCode("405");
            result.setMessage("filter不正确");
            return result;
        }
        taskParams.setFilter(filter);
        if (currentPage != null) {
            if (currentPage < 1) {
                taskParams.setCurrentPage(1);
            } else {
                taskParams.setCurrentPage(currentPage);
            }
        } else {
            taskParams.setCurrentPage(1);

        }
        if (pageSize != null) {
            if (pageSize < 1) {
                taskParams.setPageSize(4);
            } else {
                taskParams.setPageSize(pageSize);
            }
        } else {
            taskParams.setPageSize(4);
        }
        LOG.info(taskParams.getFilter());
        int counts = taskDAO.findTaskCountsWithFilter(taskParams.getFilter());
        LOG.info(String.valueOf(counts));
        paging.setPageNum(taskParams.getCurrentPage());
        paging.setPageSize(taskParams.getPageSize());
        paging.setTotalCount(counts);
        paging.setTotalPage((int) Math.ceil(counts / (taskParams.getPageSize() * 1.0)));
        List<TaskDO> taskDOS;
        taskDOS = taskDAO.pageQuery(taskParams);
        List<Task> tasks = new ArrayList<>();
        if (!CollectionUtils.isEmpty(taskDOS)) {
            taskDOS.forEach(taskDO -> tasks.add(taskDO.toModel()));
        }
        paging.setData(tasks);
        result.setData(paging);
        result.setSuccess(true);
        return result;
    }
}
