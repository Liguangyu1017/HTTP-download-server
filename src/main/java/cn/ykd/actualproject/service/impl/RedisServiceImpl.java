package cn.ykd.actualproject.service.impl;

import cn.ykd.actualproject.dao.SettingsDAO;
import cn.ykd.actualproject.dao.TaskDAO;
import cn.ykd.actualproject.dataobject.TaskDO;
import cn.ykd.actualproject.model.Task;
import cn.ykd.actualproject.model.TaskStatus;
import cn.ykd.actualproject.service.RedisService;
import cn.ykd.actualproject.service.DownloadService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

@Service
public class RedisServiceImpl implements RedisService {

    @Autowired
    private RedisTemplate redisTemplate;

    @Autowired
    private DownloadService downloadService;

    @Autowired
    private SettingsDAO settingsDAO;

    @Autowired
    private TaskDAO taskDAO;

    private static final Logger LOG = LoggerFactory.getLogger(RedisServiceImpl.class);

    private static final String KEY_MESSAGE_DOWNLOAD = "DOWNLOAD_QUEUE";
    private static final String KEY_MESSAGE_PARALLEL_DOWNLOAD = "DOWNLOAD_THREAD_QUEUE";
    private static final String KEY_MESSAGE_TASK = "TASK_QUEUE";
    private static final String KEY_CHUNK_HASHMAP = "CHUNK_HASHMAP";
    // The Url where the last download message was stored
    private String BEFORE_MESSAGE = "";


    @Override
    public void downloadExecution(TaskDO pushTaskDO) {
        // Insert message queue
        produceTaskDO(pushTaskDO);
        // Get the last message task instance
        TaskDO beforeTaskDO = taskDAO.findByUrl(BEFORE_MESSAGE);
        // Check whether the last message task exists
        if (beforeTaskDO != null) {
            // Provides a cyclic timing judgment for "last message task was' downloading 'status"
            while (true) {
                // Check whether the last message task is in the "Downloading" state
                // Not in the Downloading state
                if (!beforeTaskDO.getStatus().equals("Downloading")) {
                // A message task is displayed in the message queue
                TaskDO popTaskDO = consumeTaskDO();
                // Record this message
                    BEFORE_MESSAGE = popTaskDO.getUrl();
                    if (!downloadService.download(popTaskDO)) {
                        LOG.info(BEFORE_MESSAGE + "下载失败");
                        return;
                    }
                    LOG.info(BEFORE_MESSAGE + "下载完成");
                    // Exit the loop
                    return;
                    // Is in the Downloading state
                } else {
                    // Timing judgment
                    try {
                        LOG.info("定时2s,查询上条消息任务的状态");
                        TimeUnit.SECONDS.sleep(2);
                        beforeTaskDO = taskDAO.findByUrl(BEFORE_MESSAGE);
                    } catch (InterruptedException e) {
                        throw new RuntimeException(e);
                    }
                }
            }
            // inexistence
        } else {
            LOG.info("上条消息不存在时");
            // A message task is displayed in the message queue
            TaskDO taskDO1 = consumeTaskDO();
            // Record this message
            BEFORE_MESSAGE = taskDO1.getUrl();
            if (!downloadService.download(taskDO1)) {
                LOG.info(BEFORE_MESSAGE + "下载失败");
                return;
            }
            LOG.info(BEFORE_MESSAGE + "下载完成");
        }
    }


    @Override
    public void parallelDownloadExecution(TaskDO taskDO) {
        // Get the number of parallel tasks
        int maxTasks = settingsDAO.getMaxTasks();
        LOG.info(" Current number of parallel download tasks: "+redisTemplate.opsForHash().size(KEY_MESSAGE_PARALLEL_DOWNLOAD));
        // Update the task status to "PENDING"
        taskDO.setStatus(TaskStatus.Pending.toString());
        taskDAO.update(taskDO);
        // Wait for the download when the number of parallel tasks is full
        while (redisTemplate.opsForHash().size(KEY_MESSAGE_PARALLEL_DOWNLOAD) >= maxTasks) {
            try {
                LOG.info("并行下载任务线程池已满，等待1秒");
                TimeUnit.SECONDS.sleep(1);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
            if (redisTemplate.opsForHash().size(KEY_MESSAGE_PARALLEL_DOWNLOAD) < maxTasks) {
                LOG.info("并行下载任务线程池存在空闲");
                break;
            }
        }
        // Create a single thread
        ExecutorService testThread = Executors.newSingleThreadExecutor();
        // Save the thread instance to redis
        redisTemplate.opsForHash().put(KEY_MESSAGE_PARALLEL_DOWNLOAD, taskDO.getId(), testThread.hashCode());
        // Run a single thread
        testThread.execute(() -> {
            // Invoke the task download interface
            downloadService.download(taskDO);
            testThread.shutdownNow();
        });
    }
    @Override
    public void initializeScoreboard(String taskId,long chunkNum){
        Map<String,Boolean> scoreboard=new HashMap<>();
        for (long i = 0;i < chunkNum; i++){
            scoreboard.put(String.valueOf(i),false);
        }
        redisTemplate.opsForHash().put(KEY_CHUNK_HASHMAP,taskId,scoreboard);
    }

    @Override
    public void updateScoreboard(String taskId, long chunkId) {
        Map<String,Boolean> scoreboard=(Map) redisTemplate.opsForHash().get(KEY_CHUNK_HASHMAP,taskId);
        scoreboard.put(String.valueOf(chunkId),true);
        redisTemplate.opsForHash().put(KEY_CHUNK_HASHMAP,taskId,scoreboard);
    }

    @Override
    public List<Long> getScoreboard(String taskId){
        List<Long> chunkIds=new ArrayList<>();
        Map<String,Boolean> scoreboard=(Map) redisTemplate.opsForHash().get(KEY_CHUNK_HASHMAP,taskId);
        scoreboard.forEach((key,value)->{
            if (!value){
                chunkIds.add(Long.valueOf(key));
            }
        });
        return chunkIds;
    }

    @Override
    public boolean deleteScoreboard(String taskId){
        return redisTemplate.opsForHash().delete(KEY_CHUNK_HASHMAP,taskId)==1L;
    }

    @Override
    public void callbackExecution(TaskDO taskDO) {
        produceTask(taskDO.toModel());
        LOG.info("redis消息列表返回进度：" + taskDO.getProgress());
    }

    // taskDOProducer
    public void produceTaskDO(TaskDO taskDO) {
        while (true) {
            //Whether the insertion is successful
            if (redisTemplate.opsForList().rightPush(KEY_MESSAGE_DOWNLOAD, taskDO) > 0L) {
                LOG.info("taskDO消息队列插入消息：" + taskDO.getUrl());
                taskDO.setStatus("PENDING");
                taskDAO.update(taskDO);
                return;
            } else {
                LOG.info("taskDO消息队列已满");
                try {
                    LOG.info("定时4s,等待插入taskDO消息队列");
                    TimeUnit.SECONDS.sleep(4);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }
        }
    }
    // taskDOConsumer
    public TaskDO consumeTaskDO() {
        TaskDO taskDO = (TaskDO) redisTemplate.opsForList().leftPop(KEY_MESSAGE_DOWNLOAD);
        if (taskDO != null) {
            LOG.info("taskDO消息队列弹出消息：" + taskDO.getUrl());
        } else {
            LOG.info("taskDO消息队列无数据");
        }
        return taskDO;
    }

    // taskProducer
    public void produceTask(Task task) {
        while (true) {
            if (redisTemplate.opsForList().rightPush(KEY_MESSAGE_TASK, task) > 0L) {
                LOG.info("task消息队列插入消息：" + task.getUrl());
                return;
            } else {
                LOG.info("task消息队列已满");
                try {
                    LOG.info("定时4s，等待插入task消息队列");
                    TimeUnit.SECONDS.sleep(4);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
                LOG.info("task消息队列插入消息失败");
            }
        }
    }
    // taskConsumer

    public Task consumeTask() {
        Task task = (Task) redisTemplate.opsForList().leftPop(KEY_MESSAGE_TASK);
        if (task != null) {
            LOG.info("task消息队列弹出消息：" + task.getUrl());
        } else {
            LOG.info("task消息队列无数据");
        }
        return task;
    }
}
