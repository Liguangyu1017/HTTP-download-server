package cn.ykd.actualproject.service.impl;

import cn.ykd.actualproject.param.ThreadsParams;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.connection.Message;
import org.springframework.data.redis.connection.MessageListener;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;

@Component
public class RedisSubscribersThreads implements MessageListener {
    @Autowired
    private RedisTemplate redisTemplate;
    private static final Logger LOG = LoggerFactory.getLogger(RedisSubscribersThreads.class);
    /**
     * @apiNote messageHandling
     */
    @Override
    public void onMessage(Message message, byte[] pattern) {
        String jsonMessage=new String(message.getBody());
        ThreadsParams threadsParams=new ThreadsParams(jsonMessage);
        redisTemplate.opsForHash().put(RedisProducer.THREADS_KEY,threadsParams.getId(), threadsParams.getThreads());
        LOG.info(RedisProducer.THREADS_KEY+"通道监听数据：");
        LOG.info("id -- "+threadsParams.getId());
        LOG.info("threads -- " +threadsParams.getThreads());
    }

    /**
     * @param id taskId
     * @return threads threadCount
     **/
    public Integer getThreadsForId(String id){
        try {
            TimeUnit.MILLISECONDS.sleep(500);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        if(redisTemplate.opsForHash().hasKey(RedisProducer.THREADS_KEY,id)){
            Integer threads= (Integer) redisTemplate.opsForHash().get(RedisProducer.THREADS_KEY,id);
            redisTemplate.opsForHash().delete(RedisProducer.THREADS_KEY,id);
            LOG.info("getThreadsForId获得数据："+threads);
            return threads;
        }else {
            LOG.info("未查询到"+id+"的更新线程数");
            return null;
        }
    }

}
