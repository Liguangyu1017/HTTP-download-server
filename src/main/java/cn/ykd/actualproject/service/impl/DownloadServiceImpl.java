package cn.ykd.actualproject.service.impl;

import cn.ykd.actualproject.config.UrlConfig;
import cn.ykd.actualproject.dao.SettingsDAO;
import cn.ykd.actualproject.dao.TaskDAO;
import cn.ykd.actualproject.dataobject.TaskDO;
import cn.ykd.actualproject.model.Settings;
import cn.ykd.actualproject.service.RedisService;
import cn.ykd.actualproject.service.TaskManagerService;
import cn.ykd.actualproject.service.DownloadService;

import cn.ykd.actualproject.utils.SharedVariables;
import com.google.common.util.concurrent.RateLimiter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicLong;


@Service
public class DownloadServiceImpl implements DownloadService {
    @Autowired
    private TaskManagerService taskManagerService;
    @Autowired
    private RedisService redisService;
    @Autowired
    private SettingsDAO settingsDAO;
    @Autowired
    private UrlConfig urlConfig;
    @Autowired
    private TaskDAO taskDAO;
    private static final Logger LOG = LoggerFactory.getLogger(DownloadServiceImpl.class);
    // Secure parallel downloads
    private static final ConcurrentHashMap<String, AtomicLong> totalDownloadedBytesMap = new ConcurrentHashMap<>();
    private static final Object lock = new Object();
    private static long[] chunkDownloadBytes;
    private static long[] chunkDownloadBytesShould;

    @Override
    public boolean download(TaskDO taskDO) {
        String fileName = taskDO.getName();
        int threadNum = taskDO.getThreads();
        String urlString = taskDO.getUrl();

        // get settings
        Settings settings = settingsDAO.get().convertToModel();
        double maxDownloadSpeedMBps = settings.getMaxDownloadSpeed();
        if (maxDownloadSpeedMBps <= 0) {
            maxDownloadSpeedMBps = 1e9;
        }
        String savePath = settings.getDownloadPath();

        RateLimiter rateLimiter = RateLimiter.create(maxDownloadSpeedMBps * 1000 * 1000);

        AtomicLong totalDownloadedBytes = new AtomicLong(0);
        totalDownloadedBytesMap.put(taskDO.getId(), totalDownloadedBytes);

        try {
            URL url = new URL(urlString);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();

            long fileSize = connection.getContentLength();
            double size = (double) fileSize / 1024 / 1024;

            taskDO.setPath(settings.getDownloadPath());
            taskDO.setSize(String.format("%.2f", size) + " MB");
            taskDO.setStatus("Downloading");
            taskDAO.update(taskDO);

            long chunkSize = determineChunkSize(fileSize);
            long chunkNum = (long) Math.ceil((double) fileSize / (double) chunkSize);

            chunkDownloadBytes = new long[(int) chunkNum];
            chunkDownloadBytesShould = new long[(int) chunkNum];

            LOG.info("fileSize = " + fileSize + ",chunkSize = " + chunkSize + ",chunkNum = " + chunkNum);

            ExecutorService executor = Executors.newFixedThreadPool(threadNum);

            File outputFile = new File(savePath, fileName);

            // Initialize the state of each shard
            redisService.initializeScoreboard(taskDO.getId(), chunkNum);

            long[] sp = new long[(int) chunkNum];
            long[] ep = new long[(int) chunkNum];

            long startTime = System.currentTimeMillis();

            for (int i = 0; i < chunkNum; i++) {
                long downloadedBytesOneChunk = chunkDownloadBytes[i];

                sp[i] = chunkSize * i;
                ep[i] = (i < chunkNum - 1) ? (sp[i] + chunkSize) - 1 : fileSize - 1;

                chunkDownloadBytesShould[i] = ep[i] - sp[i] + 1;

                executor.submit(new Download(urlString, outputFile.getPath(), sp[i], ep[i],
                        totalDownloadedBytesMap.get(taskDO.getId()), downloadedBytesOneChunk, i, rateLimiter, taskDO.getId(), redisService));
            }

            taskManagerService.updateDownloadStatus(executor, totalDownloadedBytesMap.get(taskDO.getId()), fileSize,
                    taskDO, startTime, outputFile,
                    chunkNum, chunkSize, chunkDownloadBytes, chunkDownloadBytesShould, rateLimiter, sp, ep);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    static class Download implements Runnable {
        private String fileURL;
        private String outputFile;
        private long sp;
        private long ep;
        private AtomicLong totalDownloadedBytes;
        private long downloadedBytes;
        private int chunkId;
        private RateLimiter rateLimiter;
        private String taskId;
        private RedisService redisService;

        public Download(String fileURL, String outputFile, long sp, long ep, AtomicLong totalDownloadedBytes,
                        long downloadedBytes, int chunkId, RateLimiter rateLimiter, String taskId, RedisService redisService) {
            this.fileURL = fileURL;
            this.outputFile = outputFile;
            this.sp = sp;
            this.ep = ep;
            this.totalDownloadedBytes = totalDownloadedBytes;
            this.downloadedBytes = downloadedBytes;
            this.chunkId = chunkId;
            this.rateLimiter = rateLimiter;
            this.taskId = taskId;
            this.redisService = redisService;
        }

        @Override
        public void run() {
            try {
                HttpURLConnection connection = (HttpURLConnection) new URL(fileURL).openConnection();

                connection.setRequestProperty("Range", "bytes=" + sp + "-" + ep);

                BufferedInputStream in = new BufferedInputStream(connection.getInputStream());

                RandomAccessFile raf = new RandomAccessFile(new File(outputFile), "rw");

                synchronized (lock) {
                    try {
                        raf.seek(sp);
                    } catch (IOException e) {
                        LOG.error(e.toString());
                        e.printStackTrace();
                    }
                }

                int bytesRead;

                byte[] buffer = new byte[1024];

                while ((bytesRead = in.read(buffer)) != -1) {
                    synchronized (lock) {
                        if (SharedVariables.getIsPaused()) {
                            raf.close();
                            in.close();
                            break;
                        }

                        rateLimiter.acquire(bytesRead);

                        raf.write(buffer, 0, bytesRead);

                        downloadedBytes += bytesRead;
                        chunkDownloadBytes[chunkId] = downloadedBytes;

                        if (chunkDownloadBytes[chunkId] == chunkDownloadBytesShould[chunkId]) {
                            redisService.updateScoreboard(taskId, chunkId);
                        }

                        totalDownloadedBytes.addAndGet(bytesRead);
                    }
                }

                raf.close();
                in.close();
                connection.disconnect();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


    // The shard size is determined by the file size
    private int determineChunkSize(long fileSize) {
        if (fileSize <= 10 * 1024 * 1024) {
            return urlConfig.getMinChunkSize();
        } else if (fileSize <= 100 * 1024 * 1024) {
            return urlConfig.getMidChunkSize();
        } else {
            return urlConfig.getMaxChunkSize();
        }
    }
}
