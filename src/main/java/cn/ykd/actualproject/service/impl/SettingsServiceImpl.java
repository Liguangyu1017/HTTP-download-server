package cn.ykd.actualproject.service.impl;

import cn.ykd.actualproject.dao.SettingsDAO;
import cn.ykd.actualproject.dataobject.SettingsDO;
import cn.ykd.actualproject.model.Result;
import cn.ykd.actualproject.model.Settings;
import cn.ykd.actualproject.service.SettingsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SettingsServiceImpl implements SettingsService {

    @Autowired
    private SettingsDAO settingsDAO;

    private static final Logger logger = LoggerFactory.getLogger(SettingsServiceImpl.class);

    @Override
    public Result<Settings> getSettings() {
        Result<Settings> settingsResult = new Result<>();
        SettingsDO settingsDO = settingsDAO.get();
        if (settingsDO == null) {
            settingsResult.setCode("601");
            settingsResult.setMessage("配置不存在");
            return settingsResult;
        }
        settingsResult.setData(settingsDO.convertToModel());
        settingsResult.setSuccess(true);
        return settingsResult;
    }

    @Override
    public Result<Settings> updateSettings(Settings settings) {
        Result result = new Result();
        if (settings == null) {
            result.setCode("601");
            result.setMessage("配置不存在");
            result.setSuccess(false);
            return result;
        }
        if (settings.getMaxTasks() < 0 || settings.getMaxDownloadSpeed() < 0 || settings.getMaxUploadSpeed() < 0){
            result.setCode("602");
            result.setMessage("配置信息不合理");
            result.setSuccess(false);
            return result;
        }
        settingsDAO.update(new SettingsDO(settings));
        result.setData(settings);
        result.setSuccess(true);
        return result;
    }


}