package cn.ykd.actualproject.service.impl;

import cn.ykd.actualproject.param.OptionsParams;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.connection.Message;
import org.springframework.data.redis.connection.MessageListener;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.concurrent.TimeUnit;

@Component
public class RedisSubscribersTaskOptions implements MessageListener {

    @Autowired
    private RedisTemplate redisTemplate;
    private static final Logger LOG = LoggerFactory.getLogger(RedisSubscribersTaskOptions.class);
    /**
     * messageHandling
     */
    @Override
    public void onMessage(Message message, byte[] pattern) {
        String jsonMessage=new String(message.getBody());
        OptionsParams optionsParams = new OptionsParams(jsonMessage);
        List<String> ids= optionsParams.getIds();
        ids.forEach(id-> {
            redisTemplate.opsForHash().put(RedisProducer.OPTIONS_KEY, id, optionsParams.getOption());
            LOG.info(RedisProducer.OPTIONS_KEY+"通道监听数据：");
            LOG.info("ids -- "+optionsParams.getIds().toString());
            LOG.info("option -- " +optionsParams.getOption());
        });

    }

    /**
     * @param id Check the task status of the corresponding id
     * @return Options enumerates the string type data corresponding to the class element
     **/
    public String getOptionsForId(String id){
        try {
            TimeUnit.MILLISECONDS.sleep(500);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        if(redisTemplate.opsForHash().hasKey(RedisProducer.OPTIONS_KEY,id)){
            String option= (String) redisTemplate.opsForHash().get(RedisProducer.OPTIONS_KEY,id);
            redisTemplate.opsForHash().delete(RedisProducer.OPTIONS_KEY,id);
            LOG.info("getOptionsForId获得数据："+option);
            return option;
        }else {
            LOG.info("未查询到"+id+"消息");
            return null;
        }
    }
}
