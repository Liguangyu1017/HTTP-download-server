package cn.ykd.actualproject.service.impl;

import cn.ykd.actualproject.dataobject.TaskDO;
import cn.ykd.actualproject.service.SseService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.connection.Message;
import org.springframework.data.redis.connection.MessageListener;
import org.springframework.stereotype.Component;

@Component
public class RedisSubscriberCallback implements MessageListener {
    @Autowired
    private SseService sseService;
    private static final Logger LOG = LoggerFactory.getLogger(RedisSubscriberCallback.class);

    /**
     * @deprecated
     * @apiNote messageHandling
     */
    @Override
    public void onMessage(Message message, byte[] pattern) {
        String jsonMessage = new String(message.getBody());
        TaskDO receiveTaskDO = new TaskDO(jsonMessage);
        LOG.info(RedisProducer.CALLBACK_KEY + "通道监听数据：" + receiveTaskDO);

    }

}
