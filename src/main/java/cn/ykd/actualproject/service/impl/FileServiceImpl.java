package cn.ykd.actualproject.service.impl;

import cn.ykd.actualproject.dao.SettingsDAO;
import cn.ykd.actualproject.model.Result;
import cn.ykd.actualproject.param.FileParams;
import cn.ykd.actualproject.service.FileService;
import cn.ykd.actualproject.utils.FileInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.io.File;
import java.util.*;

@Service
public class FileServiceImpl implements FileService {

    @Autowired
    private SettingsDAO settingsDAO;
    private static final Logger LOG = LoggerFactory.getLogger(FileServiceImpl.class);
    private static final String separator = File.separator; // Gets the separator based on the operating system
    private String currentOrder = "up";

    @Override
    public Result<List<FileInfo>> fetchFileList(FileParams params,String fileName) {
        String rootPath = settingsDAO.get().getDownloadPath();
        Result<List<FileInfo>> result = new Result<>();
        // fileType(All,Video(*.mpv,*.mov),Photo(*.png,*.jpg,*.gif),Archive(*.zip,*.rar,*.tar),Document(*.pptx,*.docx,*.xlsx))
        String type = params.getType();
        if (type.isEmpty()) type = "All";

        // sortType(name/size/gmtCreated)
        String sort = params.getSort();
        if (sort.isEmpty()) sort = "name";

        // In positive or reverse order(up/down)
        String order = currentOrder;
        // Change the order value dynamically
        if (currentOrder.equals("up")) {
            currentOrder = "down";
        } else {
            currentOrder = "up";
        }

        List<FileInfo> fileInfos = new ArrayList<>();

        List<File> fileList = getAllFiles(rootPath + separator + fileName);

        // Filter by file type
        fileList = filterFilesByType(fileList, type);

        // Sort by sort type and order
        sortFiles(fileList, sort, order);

        // Extract a list of file information
        for (File file : fileList) {
            if (file.isDirectory()) {
                // If it is a folder, only add the name of the folder and the modification time
                fileInfos.add(new FileInfo(file.getName(), -1, new Date(file.lastModified()),true));
            } else {
                // If it is a file, add information about the file: name, size, and modification time
                fileInfos.add(new FileInfo(file.getName(), file.length(), new Date(file.lastModified()),false));
            }
        }


        LOG.info("获取文件列表成功!");
        result.setData(fileInfos);
        result.setSuccess(true);
        return result;
    }

    private List<File> getAllFiles(String path) {
        List<File> fileList = new ArrayList<>();
        File directory = new File(path);
        if (directory.exists() && directory.isDirectory()) {
            File[] files = directory.listFiles();
            if (files != null) {
                fileList.addAll(Arrays.asList(files));
            }
        }
        return fileList;
    }

    private List<File> filterFilesByType(List<File> fileList, String type) {
        List<File> filteredList = new ArrayList<>();
        for (File file : fileList) {
            boolean match = false;
            if (type.equals("All")) {
                match = true; // No type filtering. All matches
            } else if (type.equals("Video")) {
                match = file.getName().endsWith(".mp4") || file.getName().endsWith(".mov");
            } else if (type.equals("Photo")) {
                match = file.getName().endsWith(".png") || file.getName().endsWith(".jpg") || file.getName().endsWith(".gif");
            } else if (type.equals("Archive")) {
                match = file.getName().endsWith(".zip") || file.getName().endsWith(".rar") || file.getName().endsWith(".tar");
            } else if (type.equals("Document")) {
                match = file.getName().endsWith(".pptx") || file.getName().endsWith(".docx") || file.getName().endsWith(".xlsx");
            }
            if (match) {
                filteredList.add(file);
            }
        }
        return filteredList;
    }

    private void sortFiles(List<File> fileList, String sort, String order) {
        // Sort by sort type and order
        Collections.sort(fileList, new Comparator<File>() {
            @Override
            public int compare(File f1, File f2) {
                if (sort.equals("name")) {
                    return (order.equals("up")) ? f1.getName().compareTo(f2.getName()) : f2.getName().compareTo(f1.getName());
                } else if (sort.equals("size")) {
                    return (order.equals("up")) ? Long.compare(f1.length(), f2.length()) : Long.compare(f2.length(), f1.length());
                } else if (sort.equals("gmtCreated")) {
                    return (order.equals("up")) ? Long.compare(f1.lastModified(), f2.lastModified()) : Long.compare(f2.lastModified(), f1.lastModified());
                }
                return 0;
            }
        });
    }
}
