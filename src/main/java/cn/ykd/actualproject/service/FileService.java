package cn.ykd.actualproject.service;

import cn.ykd.actualproject.model.Result;
import cn.ykd.actualproject.param.FileParams;
import cn.ykd.actualproject.utils.FileInfo;

import java.io.File;
import java.util.List;

public interface FileService {
    /**
     * Gets a list of files by path and sort
     * @param params,fileName
     * @return
     */
    public Result<List<FileInfo>> fetchFileList(FileParams params,String fileName);
}
