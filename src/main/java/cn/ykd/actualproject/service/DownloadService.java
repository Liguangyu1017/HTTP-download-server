package cn.ykd.actualproject.service;

import cn.ykd.actualproject.dataobject.TaskDO;

public interface DownloadService {
    /*
     * Pass in taskdo to perform the download task
     */
    public boolean download(TaskDO taskDO);
}
