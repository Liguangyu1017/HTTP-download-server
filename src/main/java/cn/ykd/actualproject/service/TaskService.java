package cn.ykd.actualproject.service;

import cn.ykd.actualproject.model.Paging;
import cn.ykd.actualproject.model.Result;
import cn.ykd.actualproject.model.Task;
import cn.ykd.actualproject.param.UrlRequestParams;

import java.util.List;

public interface TaskService {

    /*
     * Insert a piece of data
     */
    public Result<Task> submit(UrlRequestParams urlRequestParams);


    /*
     * Update a thread of data
     */
    public Result<Task> update_thread(String id, Integer threads);

    /*
     * The task list of Paging data type obtained after filtering filter conditions
     */
    public Result<Paging<Task>> get_tasks(Integer currentPage,Integer pageSize,String filter);

    /*
     * Re-download the task in the ids list
     */
    public Result<List<Task>> refresh(List<String> ids);

    /*
     * Stop downloading tasks in the ids list
     */
    public Result<List<Task>> pause(List<String> ids);

    /*
     * Continue downloading the tasks in the ids list
     */
    public Result<List<Task>> resume(List<String> ids);

    /*
     * Delete a task from the ids list
     */
    public Result<List<Task>> delete(List<String> ids);

}
