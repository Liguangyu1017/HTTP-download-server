package cn.ykd.actualproject.service;

import cn.ykd.actualproject.dataobject.TaskDO;
import com.google.common.util.concurrent.RateLimiter;

import java.io.File;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.atomic.AtomicLong;

public interface TaskManagerService {
    /*
     * Update download status
     */
    public void updateDownloadStatus(ExecutorService executor, AtomicLong totalDownloadedBytes, long fileSize,
                                     TaskDO taskDO, long startTime,File outputFile, long chunkNum,long chunkSize,
                                     long[] chunkDownloadBytes, long[] chunkDownloadBytesShould,RateLimiter rateLimiter,
                                     long[] sp,long[] ep);
}
