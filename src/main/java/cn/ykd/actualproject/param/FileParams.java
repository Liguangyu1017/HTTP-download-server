package cn.ykd.actualproject.param;

public class FileParams {
    private String path;
    // File type
    private String type;

    // Sort type (name/size/gmtCreated)
    private String sort;

    // Positive or reverse order (up/down)
    private String order;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSort() {
        return sort;
    }

    public void setSort(String sort) {
        this.sort = sort;
    }

    public String getOrder() {
        return order;
    }

    public void setOrder(String order) {
        this.order = order;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
}
