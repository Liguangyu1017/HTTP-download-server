package cn.ykd.actualproject.param;

public class UrlRequestParams {
    private String url;

    public UrlRequestParams(String url) {
        this.url = url;
    }

    public UrlRequestParams() {
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
