package cn.ykd.actualproject.param;

import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

@Component
public class TaskParams {
    private int currentPage;
    private int PageSize;
    private String filter;
    public TaskParams(){}
    public TaskParams(int currentPage, int pageSize,String filter){
        this.currentPage=currentPage;
        this.PageSize=pageSize;
        this.filter=filter;
    }

    public String getFilter() {
        return filter;
    }

    public void setFilter(String filter) {
        this.filter = filter;
    }

    public int getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }

    public int getPageSize() {
        return PageSize;
    }

    public void setPageSize(int pageSize) {
        PageSize = pageSize;
    }
}
