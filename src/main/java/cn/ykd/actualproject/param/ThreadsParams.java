package cn.ykd.actualproject.param;

import com.fasterxml.jackson.databind.ObjectMapper;

public class ThreadsParams {

    private String id;
    private int threads;

    public ThreadsParams() {
    }

    public ThreadsParams(String id, int threads) {
        this.id = id;
        this.threads = threads;
    }

    public ThreadsParams(String jsonString) {
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            ThreadsParams threadsParams = objectMapper.readValue(jsonString, ThreadsParams.class);
            // Assigns the property parsed from the JSON string to the current object
            this.id=threadsParams.getId();
            this.threads=threadsParams.getThreads();
            // Handle other attributes as well...
            } catch (Exception e) {
            e.printStackTrace();
            // Handle exceptions
            }
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getThreads() {
        return threads;
    }

    public void setThreads(int threads) {
        this.threads = threads;
    }
}
