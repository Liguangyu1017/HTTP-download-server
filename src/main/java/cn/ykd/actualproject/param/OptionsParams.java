package cn.ykd.actualproject.param;

import cn.ykd.actualproject.model.Options;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.Serializable;
import java.util.List;

public class OptionsParams implements Serializable {
    private List<String> ids;
    private String option;

    public List<String> getIds() {
        return ids;
    }

    public void setIds(List<String> ids) {
        this.ids = ids;
    }

    public String getOption() {
        return option;
    }

    public void setOption(String option) {
        this.option = option;
    }

    public OptionsParams(){

    }

    public OptionsParams(List<String> ids, Options option) {
        this.ids = ids;
        this.option = option.toString();
    }

    public OptionsParams(String jsonString){
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            OptionsParams options = objectMapper.readValue(jsonString, OptionsParams.class);
            // Assigns the property parsed from the JSON string to the current object
            this.option=options.getOption();
            this.ids=options.getIds();
            // Handle other attributes as well...
            } catch (Exception e) {
            e.printStackTrace();
            // Handle exceptions
            }
    }
}
