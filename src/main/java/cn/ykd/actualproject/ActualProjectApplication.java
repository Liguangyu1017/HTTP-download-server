package cn.ykd.actualproject;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@MapperScan("cn.ykd.actualproject.dao")
@EnableScheduling
@SpringBootApplication
public class ActualProjectApplication {

	public static void main(String[] args) {
		SpringApplication.run(ActualProjectApplication.class, args);
	}

}
