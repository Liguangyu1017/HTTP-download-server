package cn.ykd.actualproject.dao;


import cn.ykd.actualproject.dataobject.TaskDO;
import cn.ykd.actualproject.param.TaskParams;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface TaskDAO {

    /**
     * Insert a piece of data
     */
    int insert(TaskDO taskDO);

    /**
     * Update a piece of data
     */
    int update(TaskDO taskDO);

    /**
     * Find data in the Downloading state
     */
    List<TaskDO> selectDownloadingTasks();

    /**
     * reDownload
     */
    int refresh(String id);

    /**
     * Stop downloading
     */
    int pause(String id);

    /**
     * Resume download
     */
    int resume(String id);

    /**
     * Close download
     */
    int cancel(String id);

    /**
     * Update thread
     */
    int updateThreads(@Param("id") String id, @Param("threads") int threads);

    /**
     * Update the data in the download
     */
    int updateDownloadProgress(String id, String downloadSpeed, int remainingTime, int progress);

    /**
     * Delete a piece of data
     */
    int delete(String id);

    /**
     * Delete a piece of data based on name
     */
    int deleteByName(String name);

    /**
     * Finds a piece of data based on its id
     */
    TaskDO findById(String id);

    /**
     * Finds a piece of data based on an address
     */
    TaskDO findByUrl(@Param("url") String url);

    /**
     * Find data based on the type of task
     */
    int findTaskCountsWithFilter(String filter);

    /**
     * Find data based on the url
     */
    int findNumByUrl(String url);

    /**
     * Finds all data Paging types
     */
    List<TaskDO> pageQuery(TaskParams taskParams);
}
