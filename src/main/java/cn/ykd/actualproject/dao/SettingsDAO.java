package cn.ykd.actualproject.dao;

import cn.ykd.actualproject.dataobject.SettingsDO;


public interface SettingsDAO {
    int update(SettingsDO settingsDO);

    SettingsDO get();

    Integer getMaxTasks();
}