## Other version:

This project has also written a version in go language, welcome star!

Project Address: `https://github.com/Lemon001017/HTTP-download-server`

## UI:

![img.png](src/main/resources/public/http-ui-main/public/img.png)

## How to run locally:

**Step1**: Enter ```application.properties``` ,change your database and redis configuration

**Step2**: Create table in mysql: run ```settings.sql``` and ```task.sql```

**Step3**: Start SpringBoot,run /resources/public/http/ui-main/public/index.html

